Ext.define("App.controller.SessionController", {
  extend: "Ext.app.Controller",
  stores: ["Sessions", "Presenters", "SessionPresenters"],
  refs: [
    {
      ref: "details",
      selector: "detailspanel",
    },
    {
      ref: "presenters",
      selector: "presenters",
    },
    {
      ref: "sessions",
      selector: "sessiongridpanel",
    },
  ],
  onItemDblClick: function (gridPanel, record, item, event) {
    var formWindow = Ext.create("App.view.SessionForm");
    var form = formWindow.down("form");
    form.loadRecord(record);
    formWindow.show();
  },
  onSelect: function (rowmodel, record, index, event) {
    Ext.suspendLayouts();
    var sessionId = record.get("id");
    var presenterIds = [];
    var sessionPresentersStore = this.getSessionPresentersStore();
    var presentersStore = this.getPresentersStore();
    sessionPresentersStore.each(function (rec) {
      if (rec.get("sessionId") === sessionId) {
        presenterIds.push(rec.get("presenterId"));
      }
    });
    presentersStore.clearFilter();
    presentersStore.filterBy(function (rec) {
      for (var i = 0; i < presenterIds.length; i++) {
        if (rec.get("id") === presenterIds[i]) {
          return true;
        }
      }
      return false;
    });
    // Fill details
    var sessions = record.getData();
    sessions.presenters = [];
    presentersStore.each(function (rec) {
      sessions.presenters.push(rec.getData());
    });
    var detailsPanel = this.getDetails();
    detailsPanel.update(sessions);
    Ext.resumeLayouts();
  },
  init: function () {
    this.control({
      sessiongridpanel: {
        itemdblclick: this.onItemDblClick,
        select: this.onSelect,
      },
    });
  },
});
