Ext.define("App.view.PresenterGrid", {
  extend: "Ext.grid.Panel",
  alias: "widget.presenters",
  title: "Presenter(s)",
  store: "Presenters",
  columns: [
    {
      xtype: "gridcolumn",
      dataIndex: "firstLast",
      text: "Presenter Name",
      flex: 1,
    },
    {
      xtype: "numbercolumn",
      dataIndex: "id",
      text: "id",
    },
  ],
});
