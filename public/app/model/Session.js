Ext.define("App.model.Session", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    {
      name: "title",
      sortType: "asUCText",
    },
    "approved",
    {
      name: "sessionTimeDateTime",
      type: "date",
      dateFormat: "c",
      sortType: "asDate",
    },
    {
      name: "sessionTimePretty",
      type: "string",
      convert: function (v, fields) {
        var convert = Ext.util.Format.dateRenderer("m/d/Y g:i a");
        return convert(fields.get("sessionTimeDateTime"));
      },
    },
  ],
  proxy: {
    type: "rest",
    url: "/api/sessions",
    reader: {
      type: "json",
      root: "data",
    },
  },
});
