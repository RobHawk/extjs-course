Ext.define("App.model.Presenter", {
  extend: "Ext.data.Model",
  requires: ["Ext.data.Field", "Ext.data.proxy.Rest", "Ext.data.reader.Json"],
  fields: [
    {
      name: "id",
      type: "int",
    },
    {
      name: "firstName",
    },
    {
      name: "lastName",
    },
    {
      name: "firstLast",
      convert: function (v, rec) {
        return rec.get("firstName") + " " + rec.get("lastName");
      },
    },
    {
      name: "webSite",
    },
    {
      name: "bio",
    },
    {
      name: "imageUrl",
    },
    {
      name: "imagePicture",
    },
  ],
  proxy: {
    type: "rest",
    url: "/api/presenters",
    reader: {
      type: "json",
      root: "data",
    },
  },
});
