Ext.define("App.store.Presenters", {
  extend: "Ext.data.Store",
  requires: ["App.model.Presenter", "Ext.util.Filter"],
  model: "App.model.Presenter",
  autoLoad: true,
  storeId: "Presenters",
  pageSize: 999,
  filters: {
    filterFn: function () {
      return false;
    },
  },
});
