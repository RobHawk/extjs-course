Ext.define("App.store.Sessions", {
  extend: "Ext.data.Store",
  model: "App.model.Session",
  autoLoad: true,
  autoSync: true,
  sorters: [
    {
      property: "title",
    },
  ],
  groupField: "sessionTimeDateTime",
});
