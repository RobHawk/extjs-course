Ext.define("App.store.SessionPresenters", {
  extend: "Ext.data.Store",
  requires: ["App.model.SessionPresenter", "Ext.util.Sorter"],
  autoLoad: true,
  model: "App.model.SessionPresenter",
  storeId: "SessionPresenters",
  pageSize: 9999,
  sorters: {
    property: "sequence",
  },
});
