Ext.Loader.setConfig({
  enabled: true,
});

Ext.application({
  name: "App",
  requires: ["App.view.Main"],
  controllers: ["SessionController"],
  models: ["Session", "Presenter", "SessionPresenter"],
  stores: ["Sessions", "Presenters", "SessionPresenters"],
  views: ["SessionGrid", "SessionForm", "PresenterGrid", "DetailsPanel"],
  launch: function () {
    Ext.create("App.view.Main");
  },
});
