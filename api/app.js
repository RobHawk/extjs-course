const express = require("express");
const app = express();
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.sendFile("index.html");
});

app.get("/api/sessions", (req, res) => {
  res.sendFile("sessions.json", { root: "api/data/" });
});

app.get("/api/presenters", (req, res) => {
  res.sendFile("presenters.json", { root: "api/data/" });
});

app.get("/api/sessions/presenters", (req, res) => {
  res.sendFile("sessionpresenters.json", { root: "api/data/" });
});

module.exports = app;
