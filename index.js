const app = require("./api/app");

var HTTP_PORT = 8000;

app.listen(HTTP_PORT, () => {
  console.log(`Running at port ${HTTP_PORT}`);
});
